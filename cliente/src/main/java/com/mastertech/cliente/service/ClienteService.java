package com.mastertech.cliente.service;

import com.mastertech.cliente.exceptions.ClienteNotFoundException;
import com.mastertech.cliente.models.Cliente;
import com.mastertech.cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente create(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Cliente getById(Long id) {
        Optional<Cliente> byId = clienteRepository.findById(id);

        if(!byId.isPresent()) {
            throw new ClienteNotFoundException();
        }

        return byId.get();
    }

}
