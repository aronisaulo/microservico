package com.mastertech.cliente.models.dto;

import com.mastertech.cliente.models.Cliente;
import org.springframework.stereotype.Component;

@Component
public class ClienteMapper {

    public Cliente toCustomer(CreateClienteRequest createClienteRequest) {
        Cliente cliente = new Cliente();
        cliente.setName(createClienteRequest.getName());
        return cliente;
    }

}
