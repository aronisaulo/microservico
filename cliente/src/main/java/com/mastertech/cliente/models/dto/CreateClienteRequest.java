package com.mastertech.cliente.models.dto;

public class CreateClienteRequest {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
