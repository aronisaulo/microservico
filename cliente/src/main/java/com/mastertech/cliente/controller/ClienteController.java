package com.mastertech.cliente.controller;

import com.mastertech.cliente.models.Cliente;
import com.mastertech.cliente.models.dto.CreateClienteRequest;
import com.mastertech.cliente.models.dto.ClienteMapper;
import com.mastertech.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService service;

    @Autowired
    private ClienteMapper mapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente create(@RequestBody CreateClienteRequest createClienteRequest) {
        Cliente cliente = mapper.toCustomer(createClienteRequest);

        cliente = service.create(cliente);

        return cliente;
    }

    @GetMapping("/{id}")
    public Cliente getById(@PathVariable Long id) {
        return service.getById(id);
    }

}
