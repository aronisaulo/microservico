package com.mastertech.cartao.controllers;

import com.mastertech.cartao.models.Cartao;
import com.mastertech.cartao.models.dto.*;
import com.mastertech.cartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;


    @Autowired
    private com.mastertech.cartao.models.dto.CartaoMapper mapper;

    // Maneira passada pelo Instrutor
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CreateCartaoResponse salvarCartao(@RequestBody @Valid CreateCartaoRequest createCartaoRequest){

        Cartao cartao = cartaoService.salvarCartao(createCartaoRequest);
        return mapper.toCreateCartaoResponse(cartao);
    }


    @PatchMapping("/{numero}")
    public UpdateCartaoResponse update (@PathVariable String numero, @RequestBody UpdateCartaoRequest updateCartaoRequest){
        updateCartaoRequest.setNumero(numero);
        Cartao retorno = mapper.toCartao(updateCartaoRequest);
        retorno = cartaoService.atualizaCartao(retorno);
        return mapper.toUpdateCartaoResponse(retorno);
    }

    @GetMapping("/{numero}")
    public GetCartaoResponse pequisarNumeroCartao(@PathVariable String numero){
        Cartao retorno = cartaoService.pesquisarCartao(numero);
        return mapper.toGetCartaoResponse(retorno);
    }
    @GetMapping("/id/{id}")
    public GetCartaoResponse getById(@PathVariable int id){
        Optional<Cartao> retorno = cartaoService.getById(id);
        return mapper.toGetCartaoResponse(retorno.get());
    }


}
