package com.mastertech.cartao.services;

import com.mastertech.cartao.exception.CartaoNotFoundException;
import com.mastertech.cartao.client.ClienteClient;
import com.mastertech.cartao.models.Cartao;
import com.mastertech.cartao.models.dto.ClienteDTO;

import com.mastertech.cartao.models.dto.CreateCartaoRequest;
import com.mastertech.cartao.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

     @Autowired
     CartaoRepository cartaoRepository;

     @Autowired
     private ClienteClient clienteClient;


    public Cartao salvarCartao(CreateCartaoRequest createCartaoRequest)  {

       ClienteDTO clienteDTO = clienteClient.getById(createCartaoRequest.getClienteID());
        Cartao cartao  = new Cartao();

        //System.out.println("nome:"+clienteDTO.getName());

        cartao.setClienteID(clienteDTO.getId());
        cartao.setNumero(createCartaoRequest.getNumero());
        cartao.setAtivo(false);
        return cartaoRepository.save(cartao);
    }

    public Cartao atualizaCartao(Cartao cartao) {

        Cartao databaseCartao = getByNumero(cartao.getNumero());
        databaseCartao.setAtivo(cartao.getAtivo());
        return cartaoRepository.save(databaseCartao);
    }

    public Cartao pesquisarCartao(String numero)  {
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);
         if (!cartaoOptional.isPresent()){
             throw new CartaoNotFoundException();
         }
         return cartaoOptional.get();
    }

    public Cartao getByNumero(String number) {
        /* Exemplo em 1 linha
        CreditCard creditCard = creditCardRepository.findByNumbero(numbero)
                .orElseThrow(CreditCardNotFoundException::new);
        */

        // nosso código normal
        Optional<Cartao> byId = cartaoRepository.findByNumero(number);

        if(!byId.isPresent()) {
            throw new CartaoNotFoundException();
        }

        return byId.get();
    }

    public Optional<Cartao> getById(int id){
        return cartaoRepository.findById(id);
    }

}
