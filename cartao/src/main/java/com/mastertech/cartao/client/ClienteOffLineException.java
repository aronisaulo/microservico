package com.mastertech.cartao.client;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY , reason = "Api de Cliente indisponivel no momento")
public class ClienteOffLineException extends RuntimeException {

}
