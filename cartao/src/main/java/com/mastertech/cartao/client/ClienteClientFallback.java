package com.mastertech.cartao.client;

import com.mastertech.cartao.models.dto.ClienteDTO;

public class ClienteClientFallback implements ClienteClient {
    @Override
    public ClienteDTO getById(Long id) {
        throw  new ClienteOffLineException();
    }
}
