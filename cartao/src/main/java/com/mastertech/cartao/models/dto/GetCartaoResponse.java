package com.mastertech.cartao.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetCartaoResponse {

    private int id;

    @JsonProperty("numero")
    private String numero;

    @JsonProperty("clienteId")
    private Long clienteId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Long getClienteId() {
        return clienteId;
    }

    public void setClienteId(Long clienteId) {
        this.clienteId = clienteId;
    }
}
