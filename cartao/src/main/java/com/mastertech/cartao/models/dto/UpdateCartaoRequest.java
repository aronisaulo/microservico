package com.mastertech.cartao.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpdateCartaoRequest {

    @JsonProperty("numero")
    private String numero;

    @JsonProperty("ativo")
    private Boolean ativo;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
