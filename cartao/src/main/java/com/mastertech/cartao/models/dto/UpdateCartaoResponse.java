package com.mastertech.cartao.models.dto;
import com.fasterxml.jackson.annotation.JsonProperty;
public class UpdateCartaoResponse {

    private int id;

    @JsonProperty("numero")
    private String numero;

    @JsonProperty("clienteId")
    private Long clienteId;

    @JsonProperty("ativo")
    private Boolean ativo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public Long getClienteId() {
        return clienteId;
    }

    public void setClienteId(Long clienteId) {
        this.clienteId = clienteId;
    }
}

