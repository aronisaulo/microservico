package com.mastertech.cartao.models.dto;


import com.mastertech.cartao.models.Cartao;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {

    public Cartao toCartao (CreateCartaoRequest createCartaoRequest) {
        Cartao cartao = new Cartao();
        cartao.setNumero(createCartaoRequest.getNumero());

        System.out.println(cartao.getClienteID());

        ClienteDTO clienteDTO = new ClienteDTO() ;
        clienteDTO.setId(createCartaoRequest.getClienteID());
       // cartao.setCliente(cliente);

        return cartao;
    }


    public CreateCartaoResponse toCreateCartaoResponse(Cartao cartao) {
        CreateCartaoResponse createCartaoResponse = new CreateCartaoResponse();

        createCartaoResponse.setId(cartao.getId());
        createCartaoResponse.setNumero(cartao.getNumero());
        createCartaoResponse.setClienteID(cartao.getClienteID());
        createCartaoResponse.setAtivo(cartao.getAtivo());

        return createCartaoResponse;
    }


    public Cartao toCartao(UpdateCartaoRequest updateCartaoRequest) {
        Cartao creditCard = new Cartao();

        creditCard.setNumero(updateCartaoRequest.getNumero());
        creditCard.setAtivo(updateCartaoRequest.getAtivo());

        return creditCard;
    }

    public UpdateCartaoResponse toUpdateCartaoResponse(Cartao creditCard) {
        UpdateCartaoResponse updateCartaoResponse = new UpdateCartaoResponse();

        updateCartaoResponse.setId(creditCard.getId());
        updateCartaoResponse.setNumero(creditCard.getNumero());
        updateCartaoResponse.setClienteId(creditCard.getClienteID());
        updateCartaoResponse.setAtivo(creditCard.getAtivo());

        return updateCartaoResponse;
    }

    public GetCartaoResponse toGetCartaoResponse(Cartao creditCard) {
        GetCartaoResponse getCartaoResponse = new GetCartaoResponse();

        getCartaoResponse.setId(creditCard.getId());
        getCartaoResponse.setNumero(creditCard.getNumero());
        getCartaoResponse.setClienteId(creditCard.getClienteID());

        return getCartaoResponse;
    }
}
    



