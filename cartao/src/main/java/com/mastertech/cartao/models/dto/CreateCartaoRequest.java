package com.mastertech.cartao.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class CreateCartaoRequest {
    @JsonProperty("numero")
    private String numero;
    @NotNull
    @JsonProperty("clienteId")
    private long clienteID;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public long getClienteID() {
        return clienteID;
    }

    public void setClienteID(long clienteID) {
        this.clienteID = clienteID;
    }
}
