package com.mastertech.pagamento.controller;

import com.mastertech.pagamento.models.Pagamento;
import com.mastertech.pagamento.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping("/pagamento")
    public Pagamento create(@RequestBody Pagamento pagamento) {
        return pagamentoService.create(pagamento);
    }

    @GetMapping("/pagamentos/{cartao_id}")
    public List<Pagamento> findAllByCreditCard(@PathVariable int cartao_id) {
        return pagamentoService.findAllByCreditCard(cartao_id);
    }

}
