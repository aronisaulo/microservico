package com.mastertech.pagamento.client;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "O cartao informado é inválido")
public class InvalidCartaoException extends RuntimeException {
}
