package com.mastertech.pagamento.client;

import feign.Response;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class CartaoClientDecoder implements ErrorDecoder {
    private ErrorDecoder errorDecoder =  new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            throw new InvalidCartaoException();
        } else if (response.status()==500) {
            throw new InvalidCartaoException();

        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
