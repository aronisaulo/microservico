package com.mastertech.pagamento.client;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY , reason = "Api de Cartao indisponivel no momento")
public class CartaoOffLineException extends RuntimeException {
}
