package com.mastertech.pagamento.client;

import com.mastertech.pagamento.models.dto.CartaoDTO;

public class CartaoClientFallback implements CartaoClient{
    @Override
    public CartaoDTO getCartaoById(int id) {
       throw new CartaoOffLineException();
    }
}
