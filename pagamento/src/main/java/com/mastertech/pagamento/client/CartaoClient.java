package com.mastertech.pagamento.client;

import com.mastertech.pagamento.models.dto.CartaoDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="cartao" , configuration = CartaoClientConfiguration.class)
public interface CartaoClient {
    @GetMapping("/cartao/id/{id}")
    CartaoDTO getCartaoById(@PathVariable int id);
}
