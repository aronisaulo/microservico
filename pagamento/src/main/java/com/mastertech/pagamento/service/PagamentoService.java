package com.mastertech.pagamento.service;

import com.mastertech.pagamento.client.CartaoClient;
import com.mastertech.pagamento.models.dto.CartaoDTO;
import com.mastertech.pagamento.models.Pagamento;
import com.mastertech.pagamento.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoClient cartaoClient;

    public Pagamento create(Pagamento pagamento) {

        CartaoDTO creditCard = cartaoClient.getCartaoById(pagamento.getCredicardId());

        pagamento.setCredicardId(creditCard.getId());

        return pagamentoRepository.save(pagamento);
    }

    public List<Pagamento> findAllByCreditCard(int creditCardId) {
        CartaoDTO creditCard = cartaoClient.getCartaoById(creditCardId);
        return pagamentoRepository.findAllByCredicardId(creditCard.getId());
    }

}
