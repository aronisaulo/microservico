package com.mastertech.pagamento.repository;


import com.mastertech.pagamento.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {

    List<Pagamento> findAllByCredicardId(int credicardId);
}
