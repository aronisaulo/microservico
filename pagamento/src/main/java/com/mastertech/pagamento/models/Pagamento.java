package com.mastertech.pagamento.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mastertech.pagamento.models.dto.CartaoDTO;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table
public class Pagamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @JsonProperty("descricao")
    private String description;

    @Column
    @JsonProperty("valor")
    private BigDecimal value;
    @Column
    @JsonProperty("cartao_id")
    private int credicardId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public int getCredicardId() {
        return credicardId;
    }

    public void setCredicardId(int credicardId) {
        this.credicardId = credicardId;
    }
}
